import { Module } from "@nestjs/common";
import { TwitterController } from "./controllers/twitter.controller";
import { TwitterService } from "./services/twitter.service";
import { MongooseModule } from "@nestjs/mongoose";
import { TwitterAccountSchema } from "./schemas/twitter_account.schema";
import { RequestService } from "./services/request.service";
import { TwitterTokenSchema } from "./schemas/twitter_token.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "TwitterAccount", schema: TwitterAccountSchema },
      { name: "TwitterToken", schema: TwitterTokenSchema }
    ])
  ],
  controllers: [TwitterController],
  providers: [TwitterService, RequestService],
  exports: [
    MongooseModule
  ]
})
export class TwitterModule {
}
