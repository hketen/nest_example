import { Controller, Get, Query, Session } from "@nestjs/common";
import { TwitterService } from "../services/twitter.service";
import { Auth } from "../../common/decorators";

@Controller("twitter")
export class TwitterController {

  /*
   * --> request token
   * <-- URL
   * --> open URL
   * <--
   */

  constructor(private readonly twitterService: TwitterService) {
  }

  @Get("get-url")
  @Auth()
  async getRequestTokenUrl(@Session() { userId }) {

    console.log("userId: ", userId);

    const { url } = await this.twitterService.requestToken(userId);

    return { url };
  }

  @Get("callback")
  async callback(
    @Query("oauth_token") oauthToken,
    @Query("oauth_verifier") oauthVerifier
  ) {

    await this.twitterService.verify(oauthToken, oauthVerifier);

    return "<b>Account Added!</b>";
  }

}
