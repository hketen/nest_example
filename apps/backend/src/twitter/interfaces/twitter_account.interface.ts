export interface TwitterAccount {

  _id: string;
  system_user: string;
  oauth_token: string;
  oauth_token_secret: string
  user_id: string
  screen_name: string
}
