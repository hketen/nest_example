export interface TwitterToken {

  oauth_token: string;
  user_id: string
}
