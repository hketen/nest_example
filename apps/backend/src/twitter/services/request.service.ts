import { Injectable } from "@nestjs/common";
import { OAuth } from "oauth";

const consumer = {
    key: 'vpo5nPD9dC3NQRmIFILS2nxLM',
    secret: 'mePpTWYzM2t5oPRj9f6oe2G5fgzuJ3XZyKNNbbL22CU21rY05B'
};

@Injectable()
export class RequestService {

    urlToJson(data) {
        return decodeURIComponent(data)
            .split('&')
            .reduce((previousValue: any, param: string) => {

                const item = param.split('=');

                return {
                    ...previousValue,
                    [item[0]]: decodeURIComponent(item[1]) || null
                };
            }, {});
    }

    jsonToUrl(data) {
        return Object.keys(data)
          .map((key) => `${key}=${encodeURIComponent(data[key])}`)
            .join('&')
    }

    prepareUrl(requestUrl: string) {

        if (requestUrl.substring(0, 4) !== 'http') {
            requestUrl = 'https://api.twitter.com/1.1/' + requestUrl + '.json';
        }

        return requestUrl;
    }

    tryParse(result: string) {

        try {

            return JSON.parse(result);
        } catch (e) {

            return null;
        }

    }

    private static getOAuth() {
        return new OAuth(
            'https://api.twitter.com/oauth/request_token',
            'https://api.twitter.com/oauth/access_token',
            consumer.key,
            consumer.secret,
            '1.0A',
            null,
            'HMAC-SHA1'
        );
    }

    private callback(resolve, reject, jsonParse: boolean = true) {
        return (err, result: string | undefined) => {

            if (err) {

                console.log('////////////////////////////////////////');
                console.log('error', err);
                console.log('////////////////////////////////////////');

                return reject(err);
            }

            console.log('jsonParse', jsonParse);
            console.log('////////////////////////////////////////');
            console.log(err);
            console.log('////////////////////////////////////////');
            console.log(result);
            console.log('////////////////////////////////////////');

            jsonParse
                ? resolve(this.tryParse(result))
                : resolve(result);
        };
    }

    async request(method: 'get' | 'post', url: string, {oauth_token = '', oauth_token_secret = ''}, data = {}, jsonParse = true) {
        const oAuth = RequestService.getOAuth();

        return new Promise((resolve, reject) => {

            switch (method) {
                case "get":
                    oAuth.get(
                        `${this.prepareUrl(url)}${this.jsonToUrl(data)}`,
                        oauth_token,
                        oauth_token_secret,
                        this.callback(resolve, reject, jsonParse)
                    );
                    break;
                case "post":
                default:
                    oAuth.post(
                        `${this.prepareUrl(url)}`,
                        oauth_token,
                        oauth_token_secret,
                        data,
                        'application/json',
                        this.callback(resolve, reject, jsonParse)
                    );
                    break;
            }
        });
    }

    async get({url, oauth_token = '', oauth_token_secret = '', data = {}, jsonParse = true}) {

        return this.request('get', url, {oauth_token, oauth_token_secret}, data, jsonParse);
    }

    async post({url, oauth_token = '', oauth_token_secret = '', data = {}, jsonParse = true}) {

      return this.request('post', url, {oauth_token, oauth_token_secret}, data, jsonParse);
    }
}
