import { Injectable, NotFoundException } from "@nestjs/common";
import { RequestService } from "./request.service";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { TwitterAccount } from "../interfaces/twitter_account.interface";
import { TwitterToken } from "../interfaces/twitter_token.interface";

const request_token_url = "https://api.twitter.com/oauth/request_token";

@Injectable()
export class TwitterService {

  constructor(
    private readonly requestService: RequestService,
    @InjectModel("TwitterAccount") private readonly twitterAccountModel: Model<TwitterAccount>,
    @InjectModel("TwitterToken") private readonly twitterTokenModel: Model<TwitterToken>
    // @InjectModel('User') private readonly userModel: Model<User>,
  ) {
  }

  async requestToken(userId: string) {

    const request_token = await this.requestService.post({
      url: request_token_url,
      jsonParse: false
    });

    await this.twitterTokenModel.create({
      user_id: userId,
      ...this.requestService.urlToJson(request_token)
    });

    return {
      // request_token,
      url: `https://api.twitter.com/oauth/authorize?${request_token}`
    };
  }

  async verify(oauthToken: string, oauthVerifier: string): Promise<void> {

    // get saved user id
    const token = await this.twitterTokenModel
      .findOne({ oauth_token: oauthToken })
      .lean();

    if (!token) {

      throw new NotFoundException("Token not found.");
    }

    // get access token
    const queryParams = this.requestService.jsonToUrl({
      oauth_token: oauthToken,
      oauth_verifier: oauthVerifier
    });
    const oauthResult = await this.requestService.post({
      url: `https://api.twitter.com/oauth/access_token?${queryParams}`,
      jsonParse: false
    });

    // create account
    const accountJson = this.requestService.urlToJson(oauthResult);
    const account: Model<TwitterAccount> = await this.twitterAccountModel.create({
      system_user: token.user_id,
      ...accountJson
    });

  }

}
