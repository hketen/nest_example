import * as mongoose from "mongoose";

export const TwitterTokenSchema = new mongoose.Schema({

  oauth_token: String,
  user_id: String
});
