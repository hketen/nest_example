import * as mongoose from "mongoose";

export const TwitterAccountSchema = new mongoose.Schema({

  system_user: String,
  oauth_token: String,
  oauth_token_secret: String,
  user_id: String,
  screen_name: String
});
