export interface TwitterAccountDto {

  readonly _id: string;
  readonly system_user: string;
  readonly oauth_token: string;
  readonly oauth_token_secret: string
  readonly user_id: string
  readonly screen_name: string
}
