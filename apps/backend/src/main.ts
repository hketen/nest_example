import {NestFactory} from '@nestjs/core';
import * as session from 'express-session';
import * as uuid from 'uuid';
import {AppModule} from './app.module';

async function bootstrap() {

    const app = await NestFactory.create(AppModule);

    app.enableCors();

    app.use(session({
        store: new session.MemoryStore(),
        genid: (): string => uuid.v1(),
        secret: 'utwitwi-v2',
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: Date.now() + (7 * 86400 * 1000),
            secure: false
        }
    }));

    await app.listen(3000);
}

bootstrap();
