import {Module} from '@nestjs/common';
import {ScheduleModule} from "@nestjs/schedule";
import {JobsService} from './jobs/jobs.service';
import {TradingviewModule} from './tradingview/tradingview.module';
import {MongooseModule} from "@nestjs/mongoose";
import {TwitterModule} from './twitter/twitter.module';
import {UserModule} from './user/user.module';

@Module({
  imports: [
      MongooseModule.forRoot('mongodb://utwitwi:408Ut617@ds217099.mlab.com:17099/utwitwi-v2', {
          useUnifiedTopology: true,
          useNewUrlParser: true
      }),
      ScheduleModule.forRoot(),
      TradingviewModule,
      TwitterModule,
      UserModule
  ],
  controllers: [],
  providers: [
      JobsService
  ],
})
export class AppModule {
}
