import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from 'mongoose';
import {User} from "../interfaces/user.interface";
import {ExceptionHandler} from "@nestjs/core/errors/exception-handler";

@Injectable()
export class UserService {

    constructor(
        @InjectModel('User') private readonly model: Model<User>,
    ) {
    }


    async findWithId(id: string): Promise<User> {

        return this.model
            .findOne({_id: id})
            .lean();
    }

    async login(email: string, password: string): Promise<User> {

        return this.model
            .findOne({email, password})
            .lean();
    }

    async register(email: string, password: string) {

        const user = await this.model.findOne({email});

        console.log("user: ", user);

        if (user) {
            throw  new BadRequestException('User defined.');
        }

        return this.model.create({email, password});
    }

}
