export interface User {

    _id: string;
    full_name: string;
    email: string;
    password: string;
    profile_image: string;
    accounts: string[];
}
