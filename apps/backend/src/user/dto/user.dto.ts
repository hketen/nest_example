export interface UserDto {

    readonly _id: string,
    readonly full_name: string,
    readonly email: string,
    readonly password: string,
    readonly profile_image: string,
    readonly accounts: string[],
}
