import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "./schemas/user.schema";
import { UserController } from "./controllers/user.controller";
import { UserService } from "./services/user.service";
import { TwitterModule } from "../twitter/twitter.module";
import { TradingviewModule } from "../tradingview/tradingview.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "User", schema: UserSchema }
    ]),
    TwitterModule,
    TradingviewModule
  ],
  controllers: [UserController],
  providers: [
    UserService
  ],
  exports: [
    UserService,
    MongooseModule
  ]
})
export class UserModule {
}
