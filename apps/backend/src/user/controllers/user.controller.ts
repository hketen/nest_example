import {BadRequestException, Body, Controller, NotFoundException, Post, Session} from '@nestjs/common';
import {Model} from 'mongoose';
import {omit} from 'lodash';
import {UserService} from "../services/user.service";
import {Auth} from "../../common/decorators/auth.decorator";
import {InjectModel} from "@nestjs/mongoose";
import {User} from "../interfaces/user.interface";
import {CurrentUser} from "../../common/decorators/current-user.decorator";

@Controller()
export class UserController {

    constructor(
        @InjectModel('User') private readonly userModel: Model<User>,
        private readonly userService: UserService
    ) {
    }

    @Post('login')
    async login(
        @Body("email") email,
        @Body("password") password,
        @Session() session
    ) {

        const user = await this.userService.login(email, password);

        if (!user) {

            throw new NotFoundException("User not found.");
        }

        session['userId'] = user._id;

        return omit(user, ["password", "__v"]);
    }

    @Post('register')
    async register(
        @Body("email") email,
        @Body("password") password,
        @Body("passwordRepeat") passwordRepeat
    ) {

        if (password !== passwordRepeat) {

            throw new BadRequestException("Password not equals.")
        }

        await this.userService.register(email, password);

        return {
            success: true,
        }
    }

    @Post('me')
    @Auth()
    async check(@CurrentUser() user) {

        return omit(user, ["password", "__v"]);
    }

    @Post('logout')
    @Auth()
    async logout(@Session() session) {

        delete session['userId'];

        return {success: true}
    }

}
