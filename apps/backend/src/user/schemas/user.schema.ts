import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({

    full_name: String,
    email: {
        type: String,
        require: true,
        unique: true,
        trim: true,
    },
    password: {
        type: String,
        require: true,
    },
    profile_image: String,
    accounts: [{
        type: String,
        ref: "Twitter",
    }]
});
