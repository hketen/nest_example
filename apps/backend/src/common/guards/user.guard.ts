import { CanActivate, ExecutionContext, Inject, Injectable, NotFoundException, Optional } from "@nestjs/common";
import { UserService } from "../../user/services/user.service";

@Injectable()
export class UserGuard implements CanActivate {

  constructor(
    @Optional() @Inject("UserService") private readonly userService: UserService
  ) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {

    if (!this.userService) {

      return true;
    }

    const req = context.switchToHttp().getRequest();

    const user = this.userService.findWithId(req.session.userId);

    if (!user) {
      throw new NotFoundException("User not found.");
    }

    req.user = user;

    return true;
  }
}
