import {CanActivate, ExecutionContext, Injectable, UnauthorizedException} from '@nestjs/common';
import {Observable} from 'rxjs';

@Injectable()
export class SessionGuard implements CanActivate {

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {

        const {session} = context.switchToHttp().getRequest();

        if (!session["userId"]) {
            throw new UnauthorizedException();
        }

        return true;
    }
}
