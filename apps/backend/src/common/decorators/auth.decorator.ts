import { applyDecorators, UseGuards } from "@nestjs/common";
import { SessionGuard, UserGuard } from "../guards";

export const Auth = (roles?: string[]) => {
  return applyDecorators(
    UseGuards(SessionGuard),
    UseGuards(UserGuard)
  );
};
