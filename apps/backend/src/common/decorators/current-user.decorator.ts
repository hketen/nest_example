import { createParamDecorator } from "@nestjs/common";

export const CurrentUser = createParamDecorator(async (field: string, {user}) => {

    return user
        ? field ? user[field]: user
        : null;
});
