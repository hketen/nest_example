import {Injectable} from '@nestjs/common';
import {Cron} from '@nestjs/schedule';

@Injectable()
export class JobsService {

    @Cron('45 * * * * *')
    handleJob() {

    }
}
