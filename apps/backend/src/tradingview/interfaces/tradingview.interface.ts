export interface Tradingview {

    title: string,
    image: string,
    url: string,
    timestamp: number,
    description: string
}
