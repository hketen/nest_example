import * as mongoose from 'mongoose';

export const TradingviewSchema = new mongoose.Schema({
    title: String,
    url: {
        type: String,
        unique: true
    },
    image: String,
    timestamp: Number,
    description: String,
});
