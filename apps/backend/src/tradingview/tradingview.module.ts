import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { TradingviewController } from "./controllers/tradingview.controller";
import { TradingviewService } from "./services/tradingview.service";
import { MongooseModule } from "@nestjs/mongoose";
import { TradingviewSchema } from "./schemas/tradingview.schema";
import { TwitterModule } from "../twitter/twitter.module";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Tradingview", schema: TradingviewSchema }
    ]),
    TwitterModule
  ],
  controllers: [TradingviewController],
  providers: [
    TradingviewService
  ],
  exports: [
    MongooseModule
  ]
})
export class TradingviewModule implements NestModule {
    configure(consumer: MiddlewareConsumer): any {
    }
}
