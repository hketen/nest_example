export interface TradingviewDto {

    readonly title: string,
    readonly image: string,
    readonly url: string,
    readonly timestamp: number,
    readonly description: string
}
