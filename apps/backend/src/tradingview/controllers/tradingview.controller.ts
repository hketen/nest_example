import {Controller, Get} from '@nestjs/common';
import {TradingviewService} from "../services/tradingview.service";
import {Tradingview} from "../interfaces/tradingview.interface";

@Controller('tradingview')
export class TradingviewController {

    constructor(private readonly tradigviewService: TradingviewService) {
    }

    @Get()
    async findAll(): Promise<Tradingview[]> {

        return this.tradigviewService.findAll();
    }

    @Get("resolver")
    async findContents(): Promise<{ success: boolean }> {

        try {

            const contents = await this.tradigviewService.findContents([
                "btc",
                "eth"
            ]);

            return {success: true};
        } catch (e) {

            console.error('e', e);


        }

        return {success: false};
    }
}
