import {Test, TestingModule} from '@nestjs/testing';
import {TradingviewController} from './tradingview.controller';

describe('Tradingview Controller', () => {
  let controller: TradingviewController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TradingviewController],
    }).compile();

    controller = module.get<TradingviewController>(TradingviewController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
