import {Browser, launch, Page} from 'puppeteer';
import {existsSync, mkdirSync} from 'fs';
import {kebabCase} from 'lodash';
import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from 'mongoose';
import {Tradingview} from "../interfaces/tradingview.interface";
import {TradingviewDto} from "../dto/tradingview.dto";

@Injectable()
export class TradingviewService {

    baseUrl: string = "https://tr.tradingview.com";

    browser: Browser;

    constructor(
        @InjectModel('Tradingview') private readonly model: Model<Tradingview>,
        @InjectModel('TwitterAccount') private readonly twitterAccountModel: Model<Tradingview>
    ) {
    }

    async findAll(): Promise<Tradingview[]> {
        return this.model.find();
    }

    private async findOneWithUrl(url: string): Promise<Tradingview[]> {
        return this.model.findOne({url});
    }

    async create(item: TradingviewDto): Promise<Tradingview[]> {

        return this.model.create(item);
    }

    private async launch() {

        if (this.browser) {
            return;
        }

        this.browser = await launch({
            headless: true,
            args: [
                "--disable-gpu",
                "--disable-setuid-sandbox",
                "--force-device-scale-factor",
                "--ignore-certificate-errors",
                "--no-sandbox",
                '--disable-dev-shm-usage',
            ],
            defaultViewport: null,
            devtools: false,
            ignoreHTTPSErrors: true,
            handleSIGINT: false,
        });
    }

    async findContents(ideas: string[], search?: string): Promise<void> {

        // restart browser
        await this.die();
        await this.launch();

        if (!existsSync("temp")) {

            mkdirSync("temp");
        }

        for (const idea of ideas) {

            const urls = await this.getUrls(idea, search);

            for (const url of urls) {

                const exists = await this.findOneWithUrl(url);

                if (exists) {

                    continue;
                }

                const content = await this.getIdeaContent(`${url}`);

                await this.create(content);
            }
        }

        await this.die();
    }

    private async getIdeaContent(url: string): Promise<Tradingview> {

        const page = await this.browser.newPage();

        await page.setViewport({width: 1366, height: 768});
        await page.goto(`${this.baseUrl}${url}`, {
            waitUntil: 'networkidle0', timeout: 0
        });

        const content = await page.evaluate(() => {

            const values: any = {};

            // get title
            const titleEl = document.querySelector(".tv-chart-view__title-name");
            values.title = titleEl.textContent.trim();

            // get content timestamp.
            const dateEl = document.querySelector(".tv-chart-view__title-time");
            values.timestamp = parseInt(dateEl.getAttribute("data-timestamp"));

            // get description text
            const descriptionEl = document.querySelector(".tv-chart-view__description");
            values.description = descriptionEl.textContent.trim();

            document.querySelector("#load-more-button").remove();

            return values;
        });

        await this.screenshotDOMElement(page, {
            padding: 20,
            path: content.image = `temp/${kebabCase(content.title)}.jpg`,
            selector: ".chart-container"
        });

        await page.close();

        return {
            url,
            ...content
        };
    }

    private async getUrls(idea: string, search?: string) {

        const page: Page = await this.browser.newPage();

        await page.goto(`${this.baseUrl}/ideas/${idea}`, {
            waitUntil: 'networkidle0', timeout: 0
        });

        let titles = await page.evaluate((searchText) => {
            const items = document.querySelectorAll(".tv-widget-idea__title-row a");
            const values = [];

            for (const item of items) {
                if (!item || !item.textContent) {
                    continue;
                }

                if (searchText && !item.textContent.toLowerCase().includes(searchText)) {
                    continue;
                }

                values.push(item.getAttribute("href"));
            }

            return values;
        }, search && search.trim().toLocaleLowerCase());

        await page.close();

        return titles;
    }

    /**
     * Takes a screenshot of a DOM element on the page, with optional padding.
     *
     * @param page
     * @param {!{path:string, selector:string, padding:(number|undefined)}=} opts
     * @return {!Promise<!Buffer>}
     */
    async screenshotDOMElement(page: Page, opts: { padding?: number, path?: string, selector: string }) {
        if (opts && !opts.selector) {
            throw Error('Please provide a selector.');
        }

        const padding = 'padding' in opts ? opts.padding : 0;
        const path = 'path' in opts ? opts.path : null;
        const selector = opts.selector;

        const rect = await page.evaluate(selector => {
            const element = document.querySelector(selector);
            if (!element)
                return null;
            const {x, y, width, height} = element.getBoundingClientRect();
            return {left: x, top: y, width, height, id: element.id};
        }, selector);

        if (!rect) {
            throw Error(`Could not find element that matches selector: ${selector}.`);
        }

        return page.screenshot({
            path,
            clip: {
                x: rect.left - padding,
                y: rect.top - padding,
                width: rect.width + padding * 2,
                height: rect.height + padding * 2
            }
        });
    }

    private async die() {
        if (!this.browser) {
            return;
        }

        let pages = await this.browser.pages();

        for (const page of pages) {
            await page.close();
        }

        await this.browser.close();

        this.browser = null;
    }
}
